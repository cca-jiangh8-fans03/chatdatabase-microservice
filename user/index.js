const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
app.use(express.urlencoded({extended: false}))
const cors = require('cors')
app.use(cors())
app.listen(port, ()=>
    console.log(`HTTP Server with Express.js is listening on port:${port}`)
)
const MongoClient = require('mongodb').MongoClient;
const mongouri = "mongodb+srv://cca-jiangh8:jhw888@cca-jiangh8.avq5f.mongodb.net/cca-project?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongouri, { useNewUrlParser: true, useUnifiedTopology: true });
dbClient.connect(err => {
    if(err) throw err;
    console.log("Connected to the MongoDB");
});

app.get('/login/:username/:password',(req,res) => {
    
    console.log('Debug: /login route is handling the request');
    const db = dbClient.db();
    const username = req.params.username;
    const password = req.params.password;
    db.collection("users").findOne({username:username,password:password},(err,user) =>{
        if(err || !user){
            console.log(`${username}/${password} not found!`);
            res.send(JSON.stringify({status:"notfound"}));
        }
        if(user && user.username===username)
        {
            console.log(`${username}/${password} found!`)
            res.send(JSON.stringify({status: "found", profile: {username: username, fullname:user.fullname}}));
        }
    }) 
})
app.post('/signup',(req,res) =>{
    const db = dbClient.db();
    const {username,password,fullname,email} = req.body;
    console.log('/signup->req.body.username:' + username);
    console.log('/signup->req.body.password:' + password);
    if (!username || username.length < 4){
        res.send(JSON.stringify({status: "invalid"}));
    }
    if (!password || password.length < 4){
        res.send(JSON.stringify({status: "invalid"}));
    }  
    db.collection("users").findOne({username:username},(err,user) =>{
        if(user){
            res.send(JSON.stringify({status:"invalid"}))
        }
        else
        {
            let newUser = {username:username, password:password,fullname:fullname,email:email}
            db.collection("users").insertOne(newUser,(err,result)=>{
                if(err)
                {
                    res.send(JSON.stringify({status: "invalid"}))
                }
                res.send(JSON.stringify({status:"success",profile:{username: username, fullname: fullname} }))
            })
        }
    }) 
})